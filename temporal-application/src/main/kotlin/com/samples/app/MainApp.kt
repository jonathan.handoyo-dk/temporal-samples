package com.samples.app

import com.samples.app.workflow.GreetingWorkflow
import io.temporal.client.WorkflowClient
import io.temporal.client.WorkflowOptions
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.serviceclient.WorkflowServiceStubsOptions
import java.time.Duration

fun Int.minutes(): Duration = Duration.ofMinutes(this.toLong())
fun Int.seconds(): Duration = Duration.ofSeconds(this.toLong())

fun main() {

    val client = WorkflowClient.newInstance(WorkflowServiceStubs.newInstance(WorkflowServiceStubsOptions.newBuilder().build()))
    val workflow = client.newWorkflowStub(GreetingWorkflow::class.java, WorkflowOptions.newBuilder().setTaskQueue("task-queue").build())
    WorkflowClient.start { workflow.greet("Jonathan") }
}
