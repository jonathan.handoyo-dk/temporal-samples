package com.samples.app.workflow

import com.samples.platform.activities.GreetingActivity
import io.temporal.activity.ActivityOptions
import io.temporal.workflow.Workflow
import io.temporal.workflow.WorkflowInterface
import io.temporal.workflow.WorkflowMethod
import java.time.Duration

@WorkflowInterface
interface GreetingWorkflow {

    @WorkflowMethod
    fun greet(name: String)
}

class GreetingWorkflowImpl : GreetingWorkflow {

    companion object {
        private val logger = Workflow.getLogger(this::class.java)
        private val activityOptions = ActivityOptions.newBuilder().setScheduleToCloseTimeout(Duration.ofMinutes(5)).build()
    }

    private val greetingActivity = Workflow.newActivityStub(GreetingActivity::class.java, activityOptions)

    override fun greet(name: String) {
        println(">> Workflow.workflowId : ${Workflow.getInfo().workflowId}")
        println(">> Workflow.runId : ${Workflow.getInfo().runId}")

        println(">> Workflow >> greeting $name")
        Workflow.sleep(2000)

        println(">> Calling Activity")
        greetingActivity.greet(name)

        println(">> Done!")
    }
}
