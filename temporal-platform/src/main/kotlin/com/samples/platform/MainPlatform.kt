package com.samples.platform

import com.samples.app.workflow.GreetingWorkflowImpl
import com.samples.platform.activities.GreetingActivityImpl
import com.samples.platform.activities.NotificationActivityImpl
import io.temporal.client.WorkflowClient
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.serviceclient.WorkflowServiceStubsOptions
import io.temporal.worker.WorkerFactory
import java.time.Duration

fun Int.minutes(): Duration = Duration.ofMinutes(this.toLong())
fun Int.seconds(): Duration = Duration.ofSeconds(this.toLong())

/*
These are workers!
Every JVM that is going to be contributing to Temporal by providing either Workflow or Activity will need to register itself as below.
There could be multiple Workers with different combinations. You can even separate Workflow definitions and Activity definitions.
 */
fun main() {
    println(">> Platform starting")
    val options = WorkflowServiceStubsOptions.newBuilder().build()
    val service = WorkflowServiceStubs.newInstance(options)
    val client = WorkflowClient.newInstance(service)

    val factory = WorkerFactory.newInstance(client)
    val worker = factory.newWorker("task-queue")

    worker.registerWorkflowImplementationTypes(
        GreetingWorkflowImpl::class.java
    )

    worker.registerActivitiesImplementations(
        GreetingActivityImpl(),
        NotificationActivityImpl()
    )

    factory.start()

    println(">> Workers & Activities registered")
}
