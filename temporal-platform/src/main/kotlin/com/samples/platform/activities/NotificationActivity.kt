package com.samples.platform.activities

import io.temporal.activity.ActivityInterface
import io.temporal.activity.ActivityMethod
import io.temporal.workflow.Workflow

@ActivityInterface
interface NotificationActivity {

    @ActivityMethod
    fun notify(name: String)
}

class NotificationActivityImpl: NotificationActivity {

    private val logger = Workflow.getLogger(this::class.java)

    override fun notify(name: String) {
        println(">> notify: $name")
        Thread.sleep(3000)
    }

}
