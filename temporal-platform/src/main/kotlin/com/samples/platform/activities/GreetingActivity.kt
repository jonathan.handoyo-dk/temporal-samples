package com.samples.platform.activities

import io.temporal.activity.ActivityInterface
import io.temporal.activity.ActivityMethod
import io.temporal.workflow.Workflow

@ActivityInterface
interface GreetingActivity {

    @ActivityMethod
    fun greet(name: String)
}

class GreetingActivityImpl: GreetingActivity {

    private val logger = Workflow.getLogger(this::class.java)

    override fun greet(name: String) {
        println(">> greet: $name")
        Thread.sleep(2000)
    }
}
